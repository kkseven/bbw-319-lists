package ch.bbw.m319.lists;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings("AssertBetweenInconvertibleTypes")
class Lists1Test {

	@Test
	void warmupWithLists() {
		// Wir erstellen eine leere Liste, um später Strings reinzupacken.
		var list = new ArrayList<String>();
		assertEquals(CHANGEME, list.size());
		// Wir füllen die Liste
		list.add("fuu");
		list.add("bar");
		list.add("blup");
		System.out.println("our nice list looks like: " + list);
		assertEquals(CHANGEME, list.size());
		// Listen sind Nullindexiert. Das erste Element ist also an Position 0 und nicht 1.
		assertEquals(CHANGEME, list.get(1));
		// Und Position size-1 ist das letzte Element.
		assertEquals(CHANGEME, list.get(list.size() - 1));
	}

	@Test
	void warmupListSize2() {
		// new ArrayList<Object>() erlaubt uns alle möglichen Typen reinzupacken.
		var list = new ArrayList<>();
		list.add(42);
		list.add(3.141);
		list.add(3.141);
		list.add("a string");
		list.add(true);
		// Wir können elemente wider entfernen.
		list.remove(1);
		// Oder elemente an einer bestimmten Position ersetzen.
		list.set(0, 43);
		assertEquals(CHANGEME, "a string");
		// TODO: Füge weitere Elemente zur liste hinzu, sodass die nächste Assertion validiert.
		assertEquals(23, list.get(10));
	}

	@SuppressWarnings("ForLoopReplaceableByForEach")
	@Test
	void warmupListSize3() {
		// Diese Liste ist nicht modifizierbar. `.add()` oder `.remove()` gibt uns ein Laufzeitfehler.
		var list = List.of(5, 4, 3, 2, 1, 0);
		// Ein klassischer for-loop.
		for (int i = 0; i < list.size(); i++) {
			var item = list.get(i);
			System.out.println(item);
		}
		// Und genau dasselbe nochmals mit einem foreach.
		for (var item : list) {
			System.out.println(item);
		}
		assertEquals(CHANGEME, list.get(list.size() - 1));
	}

	/**
	 * @param list any sized list of integers
	 * @return true if it starts or ends with a one (1)
	 */
	boolean startsOrEndsWith1(List<Integer> list) {
		// TODO: implement me
		return false;
	}

	@Test
	void startsOrEndsWith1_test() {
		// DO NOT EDIT THIS METHOD, edit the method above instead
		assertTrue(startsOrEndsWith1(List.of(1, 2, 3)));
		assertTrue(startsOrEndsWith1(List.of(3, 2, 1)));
		assertTrue(startsOrEndsWith1(List.of(2, 2, 2, 2, 2, 2, 1)));
		assertTrue(startsOrEndsWith1(List.of(1, 1, 1)));
		assertTrue(startsOrEndsWith1(List.of(1)));
		assertFalse(startsOrEndsWith1(List.of(0, 0, 0)));
		assertFalse(startsOrEndsWith1(List.of(0, 1, 2)));
		assertFalse(startsOrEndsWith1(List.of()));
		assertFalse(startsOrEndsWith1(List.of(0)));
	}

	/**
	 * @param list any sized list of integers
	 * @return the sum of all its elements ignoring all the ones
	 */
	int sumWithout1(List<Integer> list) {
		int sum = 0;
		// TODO: implement me
		return sum;
	}

	@Test
	void sumWithout1_test() {
		assertEquals(2, sumWithout1(List.of(0, 1, 2)));
		assertEquals(0, sumWithout1(List.of(1, 1, 1)));
		assertEquals(5, sumWithout1(List.of(1, 2, 3)));
		assertEquals(0, sumWithout1(List.of()));
		assertEquals(0, sumWithout1(List.of(1)));
		assertEquals(0, sumWithout1(List.of(-2, 2)));
	}

	/**
	 * Given a list length 1 or more of Integers, return the difference between the largest and smallest values in the
	 * list.
	 *
	 * @param list a list with at least 1 element
	 * @return the difference between the largest and the smallest element
	 */
	int biggestDiff(List<Integer> list) {
		int max;
		int min;
		// TODO: implement me
		return 0;
	}

	@Test
	void biggestDiff_test() {
		assertEquals(8, biggestDiff(List.of(2, 10)));
		assertEquals(8, biggestDiff(List.of(10, 2)));
		assertEquals(3, biggestDiff(List.of(7, 6, 8, 5)));
		assertEquals(0, biggestDiff(List.of(1, 1, 1)));
		assertEquals(0, biggestDiff(List.of(2)));
	}

	/**
	 * Given a number of required elements, return a new list, starting at zero which contains a series of increasing
	 * integers without one having the digit one. e.g: 0,2,3,4,5,6,7,8,9,20,22,...
	 * <p>
	 * Tipp: (we had this before) an easy way to test this is with String.valueOf(...).contains("...") Advanced: without
	 * String conversion.
	 *
	 * @param count how many elements our list must have
	 * @return the list with <i>count</i> elements
	 */
	List<Integer> firstNumbersWithoutDigit1(int count) {
		var list = new ArrayList<Integer>();
		// TODO: implement me
		return list;
	}

	@Test
	void firstNumbersWithoutDigit1_test() {
		assertEquals(List.of(0), firstNumbersWithoutDigit1(1));
		assertEquals(List.of(0, 2, 3), firstNumbersWithoutDigit1(3));
		assertEquals(List.of(0, 2, 3, 4, 5), firstNumbersWithoutDigit1(5));
		assertEquals(List.of(), firstNumbersWithoutDigit1(0));
		var aBigList = firstNumbersWithoutDigit1(100);
		assertEquals(230, aBigList.get(aBigList.size() - 1));
	}

	final String CHANGEME = "CHANGEME";
}

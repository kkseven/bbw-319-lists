package ch.bbw.m319.lists;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("AssertBetweenInconvertibleTypes")
class Lists2Test implements WithAssertions {
	@Test
	void convertUnmodifyableLists() {
		var unmodifiableList = List.of(1, 2, 3, 4, 5);
		// would crash: unmodifiableList.remove(0);
		var modifyableList = new ArrayList<>(unmodifiableList);
		modifyableList.remove(0);
		assertThat(modifyableList.get(CHANGEME0)).isEqualTo(3);
	}

	@Test
	void mergeLists() {
		var listA = new ArrayList<>(List.of("f", "u", "u"));
		var listB = List.of("b", "a", "r");
		listA.addAll(listB);
		assertEquals(CHANGEME, listA.size());
		assertEquals(CHANGEME, listA.contains("u"));
		assertEquals(CHANGEME, listA.contains("X"));

		// there are two flavours of list.remove(...)
		// * list.remove(int): removes at a certain index
		// * list.remove(Object): removes all objects equal to Object
		listA.remove("u");
		assertEquals(CHANGEME, listA.size());
	}

	@Test
	void sortLists() {
		var list = new ArrayList<>(List.of(1, 2, 2, 0, 4));
		list.sort(Comparator.naturalOrder());
		assertEquals(1, list.get(CHANGEME0), "sorted: " + list);
		list.sort(Comparator.reverseOrder());
		assertEquals(1, list.get(CHANGEME0), "reverse sorted: " + list);
	}

	/**
	 * Gegeben sind zwei Listen ohne Duplikate. Gib alle Elemente zurück, welche in beiden Listen vorkommen. A=(1,2,3),
	 * B=(4,2) -> (2) A=(1,2), B=(3,1,2) -> (1, 2)
	 */
	List<Integer> intersection(List<Integer> listA, List<Integer> listB) {
		// TODO: implement me
		return List.of();
	}

	@Test
	void intersectionTest() {
		// DO NOT EDIT THIS METHOD, edit the method above instead
		assertThat(intersection(List.of(1, 2, 3), List.of(3))).containsExactlyInAnyOrder(3);
		assertThat(intersection(List.of(3, 2, 1), List.of(3, 4, 5))).containsExactlyInAnyOrder(3);
		assertThat(intersection(List.of(3, 2, 1), List.of(5, 4, 3))).containsExactlyInAnyOrder(3);
		assertThat(intersection(List.of(2, 0, 1), List.of(0, 2, 1))).containsExactlyInAnyOrder(0, 1, 2);
		assertThat(intersection(List.of(), List.of(1, 2, 3))).isEmpty();
		assertThat(intersection(List.of(1, 2, 3), List.of())).isEmpty();
		assertThat(intersection(List.of(1, 2, 3), List.of(4, 5, 6))).isEmpty();
	}

	/**
	 * Gegeben eine nicht-leere Liste von integer. Gib das N-grösste Element zurück. 0 ist das grösste. 1 das
	 * zweitgrösste, usw...
	 */
	int nBiggest(List<Integer> listA, int rank) {
		// TODO: implement me
		return 0;
	}

	@Test
	void nBiggestTest() {
		// DO NOT EDIT THIS METHOD, edit the method above instead
		assertThat(nBiggest(List.of(1, 2, 3), 0)).isEqualTo(3);
		assertThat(nBiggest(List.of(3, 2, 1), 0)).isEqualTo(3);
		assertThat(nBiggest(List.of(3, 1, 2), 2)).isEqualTo(1);
		assertThat(nBiggest(List.of(9, 9, 9), 1)).isEqualTo(9);
	}

	/**
	 * Gegeben eine Liste von integer, sortiere sie so, dass zuerst alle negativen Zahlen kommen absteigend sortiert und
	 * danach alle positiven Zahlen aufsteigend sortiert.
	 */
	List<Integer> sortSpecial(List<Integer> list) {
		// TODO: implement me
		return list;
	}

	@Test
	void sortSpecialTest() {
		assertThat(sortSpecial(List.of(0, -1, 1))).containsExactly(-1, 0, 1);
		assertThat(sortSpecial(List.of(-3, -2, -1, 0, 1, 2))).containsExactly(-1, -2, -3, 0, 1, 2);
		assertThat(sortSpecial(List.of(1, 2, -1))).containsExactly(-1, 1, 2);
		assertThat(sortSpecial(List.of(0))).containsExactly(0);
		assertThat(sortSpecial(List.of())).isEmpty();
	}

	/**
	 * Gegeben eine Liste von integer, gib das kleinste Subset zurück, welches die Summe 0 hat. zB Wenn keine Summe 0
	 * möglich ist, soll eine leere Liste zurück gegeben werden. (1,1,2,-3,1) -> (1,2,-3) (7,1,-1,8,0) -> (0)
	 */
	List<Integer> subarraySum0(List<Integer> list) {
		// TODO: implement me, after implementing tests
		return list;
	}

	@Test
	void subarraySum0Test() {
		// TODO: implement tests -> take a look at the sortSpecialTest() and write at least 3 different checks.
		//       Write the tests before writing the implementation: We call this Test Driven Design (TDD).
	}

	/**
	 * Lektüre: https://de.wikipedia.org/wiki/Median
	 * <p>
	 * Gegeben ist ein nicht-leerer String von Grossbuchstaben. Finde den Median seiner Zeichen als String von nur einem
	 * Zeichen. Üblicherweise wird der Median nur auf Zahlenreihen angewendet, geht aber für alle sortierbaren Listen.
	 * Wir nehmen an, dass die Mitte zwischen 'A' und 'D', 'B' ist (abgerundet).
	 * <p>
	 * Bonus: Eine allgemeine Lösung welche auch den Median von [😀,😜,🙄] kennt.
	 */
	String median(String word) {
		var characters = new ArrayList<>(Arrays.asList(word.split("")));
		// TODO: implement me
		return "TODO";
	}

	@Test
	void medianTest() {
		// DO NOT EDIT THIS METHOD, edit the method above instead
		assertThat(median("ABC")).isEqualTo("B");
		assertThat(median("ABX")).isEqualTo("B");
		assertThat(median("AC")).isEqualTo("B");
		assertThat(median("AD")).isEqualTo("B");
		assertThat(median("B")).isEqualTo("B");
		assertThat(median("AAXX")).isEqualTo("L");
		assertThat(median("AAYY")).isEqualTo("M");
		assertThat(median("ABCDEFGHIJKLMNOPQRSTUVWXYZ")).isEqualTo("M");
		var username = System.getProperty("user.name").toUpperCase().replaceAll("[^A-Z]", "");
		System.out.println("...and the median of " + username + " is " + median(username));
	}

	final String CHANGEME = "CHANGEME";
	final int CHANGEME0 = 0;
}

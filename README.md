# Listen in Java

Ziel:
- Wir nutzen das `List<>` Interface
  ([tutorial](https://www.java-tutorial.org/listen.html))
- Wir iterieren über Listen mithilfe von foreach.
- Wir lernen besonders, dass Listen Nullindexiert sind.
- Wir arbeiten mit immutable Datenstrukturen.
- Wir arbeiten mit Ansätzen von Test-Driven-Design (TDD).

Wir überspringen mit diesem Thema die Java-Arrays  ([tutorial](https://www.java-tutorial.org/arrays.html)), da diese im EE-Umfeld weniger wichtig sind. Konzeptuell sind sie allerdings gleich mächtig.

Aufgabe:
- Alle Aufgaben befinden sich in `src/test/java/.../*Test.java`.
- Eine Aufgabe ist generell erfüllt, sobald der Test "grün" ist.
- Aufgaben sind generell chronologisch zu lösen, überspringen ist aber ok.
- zu schwer oder zu einfach? Alternativübungen auf https://codingbat.com/java → Array-1 / Array-2
